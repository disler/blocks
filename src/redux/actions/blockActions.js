import {MOVE_HERO, UPDATE_BLOCK_SIZE} from "./actionTypes"

export const moveHero = (direction) => {
	return dispatch => {
		dispatch({type: MOVE_HERO, direction})
	}
}

export const updateBlockSize = (size) => {
	return dispatch => {
		dispatch({type: UPDATE_BLOCK_SIZE, size})
	}
}

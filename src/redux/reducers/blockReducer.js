import initialState from './initialState';
import * as actionTypes from '../actions/actionTypes';
import {funcHero, isValidPosition, getRandomBlock, funcBlock} from "../../methods"

export default function blockReducer(state = initialState, action) {

	let updatedHero

	switch (action.type) {
		case actionTypes.MOVE_HERO:

			const {direction} = action
			const hero = state.hero
			const blocks = state.blocks

			let newX = hero.x
			let newY = hero.y
			if (direction === 'up') {
				newY = hero.y-1
			}
			else if (direction === 'right') {
				newX = hero.x+1
			}
			else if (direction === 'down') {
				newY = hero.y+1
			}
			else if (direction === 'left') {
				newX = hero.x-1
			}

			const validPosition = isValidPosition(blocks, newX, newY)

			if (!validPosition) {
				return state
			}

			updatedHero = funcHero({...hero, x: newX, y: newY})
			
			return {...state, hero: updatedHero}
		
		case actionTypes.UPDATE_BLOCK_SIZE:

			const {size} = action
			const totalRowCol = Math.sqrt(size)
			const newBlocks = []

			for (let y = 0; y < totalRowCol; y++) {
				const newRow = []
				for (let x = 0; x < totalRowCol; x++) {
					if (x === 0 && y === 0) {
						newRow.push(funcBlock())
						continue
					}
					newRow.push(getRandomBlock())
				}
				newBlocks.push(newRow)
			}

			updatedHero = funcHero({...state.hero, x: 0, y: 0})

			return {...state, blocks: newBlocks, hero: updatedHero}

			default:
				return state;
	}
}
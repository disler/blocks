import {funcHero, funcBlock, funcCollideBlock} from '../../methods'

export default {
	blocks: [
		[funcBlock(), funcBlock(), funcBlock()],
		[funcBlock(), funcBlock(), funcCollideBlock()],
		[funcBlock(), funcBlock(), funcBlock()],
	],
	hero: funcHero()
}
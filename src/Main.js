import React, { Component } from 'react';
import Blocks from "./components/Blocks.js"
import Controls from "./components/Controls.js"
import "./block.css"

// redux stuff
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as blockActions from './redux/actions/blockActions';

class Main extends Component {

	onKeyUp = (event) => {

		const direction = {
			'w': 'up',
			'd': 'right',
			's': 'down',
			'a': 'left',
		}[event.key]

		if (!direction) {
			return
		}

		this.props.blockActions.moveHero(direction)
	}

	componentDidMount() {
		window.addEventListener('keyup', this.onKeyUp);
	}

	componentWillUnmount() {
		window.removeEventListener('keyup', this.onKeyUp);
	}

	render() {
		return (
			<div className="wrapper">
				<div className="controls">
					{
						Controls(this.props.blockActions.updateBlockSize)
					}
				</div>
				<div className="blocks-wrapper">
					<div className="blocks">
						{
							Blocks(this.props.blocks, this.props.hero)
						}
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		blocks: state.blockReducer.blocks,
		hero: state.blockReducer.hero
	};
  }
  
  function mapDispatchToProps(dispatch) {
	return {
	  blockActions: bindActionCreators(blockActions, dispatch)
	};
  }
  
  export default connect(
	mapStateToProps,
	mapDispatchToProps
  )(Main);
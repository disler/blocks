import React from 'react';

export default (funcUpdateBlockSize) => {
	return(
		<div>
			<span>Blocks </span>
			<select type="option" onChange={(event) => funcUpdateBlockSize(event.target.value)}>
				<option>9</option>
				<option>16</option>
				<option>25</option>
				<option>36</option>
				<option>45</option>
			</select>
		</div>
	)
}
import "../block.css"
import React from 'react';

export default (oBlock, heroClass, key) => {

	const collidableStyle = oBlock.collides ? 'collide' : ''

	return <div key={key} className={`block ${heroClass} ${oBlock.type} ${collidableStyle}`}></div>
}
import Block from "./Block.js"
import React from 'react';
import "../block.css"

const isHeroLocation = (hero, x, y) => hero.x === x && hero.y === y
const heroBlockClass = (hero, x, y) => isHeroLocation(hero, x, y) ? 'hero' : ''

export default (blocks, hero) => {
	return blocks.map((_row, _index) => {
		return (
			<div className="block-row" key={_index}>
				{_row.map((_block, __index) => {
					const heroClass = heroBlockClass(hero, __index, _index)
					return Block(_block, heroClass, `key-${__index}-${_index}`)
				})}
			</div>
		)
	})
}
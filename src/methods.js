export const funcBlock = (args) => ({
	type: "grass",
	collides: false,
	...args,
})

export const funcCollideBlock = (args) => funcBlock({collides: true})

export const funcHero = (args) => {
	return {
		id: 'hero',
		x: 0,
		y: 0,
		...args,
	}
}

export const isValidPosition = (blocks, x, y) => {
	if (x < 0 || y < 0) {
		return false
	}
	if (x > blocks[0].length -	1) {
		return false
	}
	if (y > blocks.length - 1) {
		return false
	}
	if (blocks[y][x].collides) {
		return false
	}
	return true
}

export const randomInt = (max) => {
	return Math.floor(Math.random() * max)
}

export const getRandomBlock = () => {
	const random = randomInt(3)
	return random < 2 ? funcBlock() : funcCollideBlock()
}
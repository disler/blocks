import React, { Component } from 'react';
import './App.css';
import Main from "./Main"
import configureStore from './redux/store/configureStore';
import {Provider} from "react-redux"

const store = configureStore()

class App extends Component {
  render() {
    return (
      <div className="App">
		<Provider store={store}>
			<Main></Main>
		</Provider>
        {/* <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header> */}
      </div>
    );
  }
}

export default App;
